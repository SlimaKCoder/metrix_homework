from django.conf import settings
from django.conf.urls import include, url
from django.contrib import admin
from django.contrib.auth.views import logout as logout_view
from auth_ex.views import login as login_view
from blog.views import index_view


urlpatterns = [
    url(r'^admin/', include(admin.site.urls)),
    url(r'^admin/', include('loginas.urls')),
    url(r'^search/', include('search.urls')),
    url(r'^login/$', login_view, name='login'),
    url(r'^logout$', logout_view, {'next_page': 'index'}, name='logout'),
    url(r'^oauth/', include('social_django.urls', namespace='social')),

    url(r'^$', index_view, name='index'),
    url(r'^blog/', include('blog.urls')),
    url(r'^articles/', include('articles.urls')),
    url(r'^products/', include('products.urls')),
    url(r'^cart/', include('cart.urls')),
]


if settings.DEBUG:
    import debug_toolbar
    urlpatterns += [
        url(r'^__debug__/', include(debug_toolbar.urls)),
    ]
