from carton.cart import Cart


# Converts Cart object to dict
def cart_to_dict(cart: Cart):
    cart_dict = dict()
    cart_dict['total'] = cart.total
    items_list = list()

    for item in cart.items:
        items_list.append({
            'product': {
                'name': item.product.name,
            },
            'price': item.price,
            'quantity': item.quantity,
            'subtotal': item.subtotal,
        })

    cart_dict['items'] = items_list
    return cart_dict
