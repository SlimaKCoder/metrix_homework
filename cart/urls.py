from django.conf.urls import url
from .views import *

urlpatterns = [
    url(r'^$', show_view, name='cart_show'),
    url(r'^add/(?P<product_id>\d+)', add_view, name='cart_add_single'),
    url(r'^add/(?P<product_id>\d+)/(?P<quantity>\d+)$', add_view, name='cart_add'),
    url(r'^remove/(?P<product_id>\d+)$', remove_single_view, name='cart_remove_single'),
    url(r'^remove/(?P<product_id>\d+)/all$', remove_view, name='cart_remove'),
    url(r'^clear/$', clear_view, name='cart_clear'),
    url(r'^order/$', order_view, name='cart_order'),
]
