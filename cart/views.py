from django.shortcuts import render, redirect
from carton.cart import Cart
from products.models import Product
from .tasks import send_order_task
from .utils import cart_to_dict


# Used to add product to cart
def add_view(request, product_id, quantity=1):
    cart = Cart(request.session)

    try:
        product = Product.objects.get(pk=product_id)
        cart.add(product, price=product.price, quantity=quantity)
    except Product.DoesNotExist:
        # We are failing silently, but it will
        # be good idea to add some logging here
        pass

    # We can choose page to redirect
    # using url param: ?next=<url>
    next_page = request.GET.get('next')
    if next_page:
        return redirect(next_page)

    return redirect('cart_show')


# Used to display cart
def show_view(request):
    return render(request, 'cart/show.html')


# Used to remove single product from cart
def remove_single_view(request, product_id):
    cart = Cart(request.session)
    try:
        product = Product.objects.get(pk=product_id)
        cart.remove_single(product)
    except Product.DoesNotExist:
        # We are failing silently, but it will
        # be good idea to add some logging here
        pass
    return redirect('cart_show')


# Used to remove whole product from cart
def remove_view(request, product_id):
    cart = Cart(request.session)
    try:
        product = Product.objects.get(pk=product_id)
        cart.remove(product)
    except Product.DoesNotExist:
        # We are failing silently, but it will
        # be good idea to add some logging here
        pass
    return redirect('cart_show')


def clear_view(request):
    cart = Cart(request.session)
    cart.clear()
    return redirect('cart_show')


def order_view(request):
    cart = Cart(request.session)
    send_order_task.delay(cart_to_dict(cart))
    cart.clear()
    return render(request, 'cart/finalized.html')
