from __future__ import absolute_import, unicode_literals
from django.template.loader import get_template
from django.core.mail import send_mail
from celery import shared_task
from django.conf import settings


# This task will send order via email
@shared_task
def send_order_task(cart: dict):
    template = get_template('cart/email/order.txt')
    send_mail(
        "New order",
        template.render({'cart': cart}),
        settings.EMAIL_HOST_USER,
        [settings.EMAIL_ORDER_RECIPIENT],
        fail_silently=False,
    )
