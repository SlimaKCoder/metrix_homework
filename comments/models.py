from django.db import models
from django.contrib.contenttypes.fields import GenericForeignKey
from django.contrib.contenttypes.models import ContentType
from articles.tasks import incr_comments_count_task as article_incr_cc_task
from blog.tasks import incr_comments_count_task as entry_incr_cc_task


class Comment(models.Model):
    body = models.TextField()
    created = models.DateTimeField(auto_now_add=True)
    # Generic key to allow m2one relation to Entry or Article
    content_type = models.ForeignKey(ContentType, on_delete=models.CASCADE)
    object_id = models.PositiveIntegerField()
    content_object = GenericForeignKey('content_type', 'object_id')

    # Method used to update related objects (called on post_save)
    @classmethod
    def update_related(cls, sender, instance, created, *args, **kwargs):
        # Run only for new Comment database object
        if created:
            # Import related objects
            from articles.models import Article
            from blog.models import Entry
            # Checking type of related object and executing tasks if is Article or Entry
            r_object = instance.content_object
            if isinstance(r_object, Entry):
                entry_incr_cc_task.delay(r_object.pk)
            elif isinstance(r_object, Article):
                article_incr_cc_task.delay(r_object.pk)
