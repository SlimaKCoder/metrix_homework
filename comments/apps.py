from django.apps import AppConfig
from django.db.models.signals import post_save
from django.utils.translation import ugettext_lazy as _


class CommentsConfig(AppConfig):
    name = 'comments'
    verbose_name = _("Comments App")

    def ready(self):
        # Apps needs to be ready if we want to import .models
        # or error "Apps aren't loaded yet." will rise
        from .models import Comment
        # We are adding function to run on every update (in db) of Comment
        post_save.connect(Comment.update_related, sender=Comment)
