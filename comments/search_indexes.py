from haystack import indexes
from .models import Comment


class CommentIndex(indexes.SearchIndex, indexes.Indexable):
    text = indexes.CharField(document=True, use_template=True)
    body = indexes.CharField(model_attr='body')
    created = indexes.DateTimeField(model_attr='created')

    def get_model(self):
        return Comment

    def index_queryset(self, using=None):
        # Used when the entire index for model is updated.
        return self.get_model().objects.all()
