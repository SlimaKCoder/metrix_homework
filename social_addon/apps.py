from django.apps import AppConfig
from django.utils.translation import ugettext_lazy as _


class SocialAddonConfig(AppConfig):
    name = 'social_addon'
    verbose_name = _("Social Addon App")
