from django.conf.urls import url
from .views import *

urlpatterns = [
    url(r'^$', index_view, name='articles_index'),
    url(r'^(?P<article_id>\d+)$', article_view, name='articles_article'),
]
