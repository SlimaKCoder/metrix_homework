from __future__ import absolute_import, unicode_literals
from django.db.models import F
from celery import shared_task


# Increases comments_count by 1 for Article specified by pk
@shared_task
def incr_comments_count_task(article_pk: int):
    from .models import Article
    Article.objects.filter(pk=article_pk).update(comments_count=F('comments_count') + 1)
