from django.shortcuts import render, get_object_or_404
from .models import Article


def index_view(request):
    # Pagination is needed, but not implemented yet
    articles = Article.objects.published().all()
    return render(request, 'articles/index.html', {'articles': articles})


# It will display one article by id with metadata (django-meta)
def article_view(request, article_id):
    queryset = Article.objects.published().select_related()
    article = get_object_or_404(queryset, pk=article_id)
    context = dict()
    context['article'] = article
    context['meta'] = article.as_meta()
    return render(request, 'articles/article.html', context)
