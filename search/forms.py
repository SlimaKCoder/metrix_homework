from haystack.forms import SearchForm, ModelSearchForm, SearchQuerySet
from articles.models import Article
from blog.models import Entry


class CustomModelSearchForm(SearchForm):
    search_models = [Entry, Article]

    def search(self):
        # Check form validation
        if not self.is_valid():
            return self.no_query_found()

        # Check if param q=? is not empty
        if not self.cleaned_data['q']:
            return self.no_query_found()

        # Build search queryset using defined search_models
        # - filter accepts not only exact matching but also
        # every word starting with same chars
        sqs = self.searchqueryset.models(*self.search_models) \
            .filter(content=self.cleaned_data['q']) \
            .filter_or(content__startswith=self.cleaned_data['q'])

        return sqs
