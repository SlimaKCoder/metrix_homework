from haystack.views import SearchView
from .forms import CustomModelSearchForm


class CustomModelSearchView(SearchView):
    def __init__(self, *args, **kwargs):
        # calling parent with our custom form class
        super(CustomModelSearchView, self).__init__(*args, form_class=CustomModelSearchForm, **kwargs)


