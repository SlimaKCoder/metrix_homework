from django.conf.urls import url
from .views import CustomModelSearchView

urlpatterns = [
    # we need to use () instead of .as_view()
    url(r'^$', CustomModelSearchView(), name='search_index')
]
