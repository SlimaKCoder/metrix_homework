from django.utils import timezone
from django.db import models
from django.contrib.contenttypes.fields import GenericRelation
from meta.models import ModelMeta
from comments.models import Comment


class EntryQuerySet(models.QuerySet):
    # Returns only published entries
    def published(self):
        return self.filter(pub_date__lte=timezone.now())


class Entry(ModelMeta, models.Model):
    title = models.CharField(max_length=30)
    body = models.TextField()
    created = models.DateTimeField(auto_now_add=True)
    modified = models.DateTimeField(auto_now_add=True)
    pub_date = models.DateTimeField(null=True)
    comments = GenericRelation(Comment)
    comments_count = models.IntegerField(default=0)

    objects = EntryQuerySet.as_manager()

    _metadata = {
        'title': 'title',
        # we can use body as description, cuz search
        # engines will trim text if too long
        'description': 'body'
    }
