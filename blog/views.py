from django.shortcuts import render, get_object_or_404
from .models import Entry


def index_view(request):
    # Pagination is needed, but not implemented yet
    entries = Entry.objects.published().all()
    return render(request, 'blog/index.html', {'entries': entries})


# It will display one entry by id with metadata (django-meta)
def entry_view(request, entry_id):
    queryset = Entry.objects.published().select_related()
    entry = get_object_or_404(queryset, pk=entry_id)
    context = dict()
    context['entry'] = entry
    context['meta'] = entry.as_meta()
    return render(request, 'blog/entry.html', context)
