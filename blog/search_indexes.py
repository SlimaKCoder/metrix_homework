from haystack import indexes
from .models import Entry


class EntryIndex(indexes.SearchIndex, indexes.Indexable):
    text = indexes.CharField(document=True, use_template=True)
    title = indexes.CharField(model_attr='title')
    body = indexes.CharField(model_attr='body')
    modified = indexes.DateTimeField(model_attr='modified')
    pub_date = indexes.DateTimeField(model_attr='pub_date')

    def get_model(self):
        return Entry

    def index_queryset(self, using=None):
        # Used when the entire index for model is updated.
        return self.get_model().objects.published()
