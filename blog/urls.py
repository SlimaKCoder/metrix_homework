from django.conf.urls import url
from .views import *

urlpatterns = [
    url(r'^$', index_view, name='blog_index'),
    url(r'^(?P<entry_id>\d+)$', entry_view, name='blog_entry'),
]
