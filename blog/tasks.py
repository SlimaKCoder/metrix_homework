from __future__ import absolute_import, unicode_literals
from django.db.models import F
from celery import shared_task


# Increases comments_count by 1 for Entry specified by pk
@shared_task
def incr_comments_count_task(entry_pk: int):
    from .models import Entry
    Entry.objects.filter(pk=entry_pk).update(comments_count=F('comments_count') + 1)
