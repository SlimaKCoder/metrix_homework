=========================
Metrix Homework
=========================

***************
TODO
***************
- add tests for all apps
- add caching for database and content
- add better error logging
- upgrade to Django 1.11 (or newer supported version)
- replace social-auth-app-django by django-allauth
- create better theme and more advanced templates
- add pagination for articles, blog and products pages
- decrease comments_count for Article and Entry on Comment delete
- move vendor statics outside project (eg. separate CDN)
- add favicon.ico to base template
- add captcha for login page
- add social login buttons to login page
- add app orders with Order model (for orders saving and better managing)
- implement ajax for cart app
- add user information to orders (display forms for anonymous users)
- send order email also to user
- add more options (eg. list of recipients, custom sender name) for order emails
- add html templates for order emails
- add forms for adding, changing and removing blog entries, articles, products

***************
Possible licensing problems
***************
- Django Suit is licensed under Creative Commons Attribution-NonCommercial 3.0 license.