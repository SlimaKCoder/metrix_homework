from django.shortcuts import render
from products.models import Product


def index_view(request):
    # We need to implement pagination
    products = Product.objects.all()
    return render(request, 'products/index.html', {'products': products})
